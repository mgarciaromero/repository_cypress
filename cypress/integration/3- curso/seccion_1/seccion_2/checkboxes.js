/// <reference types="Cypress" />

require('cypress-xpath');
//descargar extension xpath relative

describe("Nueva sección Checkbox ",()=>{

    it("Check up the checkboxes",()=>{

        cy.visit("https://demoqa.com/checkbox")
        cy.title().should("eq","ToolsQA");
        
        cy.get("[type='checkbox]").check().should('be.checked')
        cy.get("[type='checkbox]").uncheck().should('not.be.checked')

    })
})