/// <reference types="Cypress"/>

require('cypress-xpath');

/*
describe("Tests búsqueda de customer",()=>{
    context('Given I access to the NAF main page',()=>{
        beforeEach(()=>{
            cy.visit("http://naf-frontend-1210163857.eu-central-1.elb.amazonaws.com/#/interacion");
            cy.wait(2000);
        })
        context('When I click on Interactions And Individual Profile',()=>{
            beforeEach(()=>{
                cy.get('body > bsh-naf-root > div > div > bsh-naf-sidebar > div > div:nth-child(2) > div > span.icon')
                .click();
                cy.wait(2000);
                cy.get("body > bsh-naf-root > div > div > div > bsh-naf-inbox > div > div > div > bsh-naf-inbox-detail > div > div > div.inbox-detail-content > bsh-naf-customer-type-select > div > div > div > bsh-naf-outlined-card:nth-child(1) > div > p")
                .click();
                cy.wait(2000);

            })

        })
        context('And I search a valid customer and press ENTER',()=>{
            beforeEach(()=>{
                cy.get('body > bsh-naf-root > div > div > div > bsh-naf-inbox > div > div > div > bsh-naf-inbox-detail > div > div > div.inbox-detail-content > bsh-naf-customer-search > div > div.customer-search > div > input')
                .should('be.visible').type('5{enter}');
                cy.wait(2000);
            })
            it('Then I execute some actions',()=>{
                    //selector de paises
                cy.wait(2000);
                cy.get('.select').should('be.visible').select('Spain')
                cy.wait(2000);
                cy.get('.select').should('be.visible').select('United Kingdom')
                cy.wait(2000);
                cy.get('.select').should('be.visible').select('France')


                //Colapsables history y notes
                cy.wait(2000);
                cy.get('body > bsh-naf-root > div > div > div > bsh-naf-inbox > div > div > div > bsh-naf-inbox-detail > div > div > div.inbox-detail-content > bsh-naf-customer-search > div > div.customer-form > div.customer-form-side > mat-card.mat-card.mat-focus-indicator.customer-form-history.customer-form-table > mat-card-title > div.buttons-styles > button.mat-focus-indicator.table-buttons.mat-flat-button.mat-button-base > span.mat-button-wrapper > span')
                .click()
                cy.wait(2000);
                cy.get('body > bsh-naf-root > div > div > div > bsh-naf-inbox > div > div > div > bsh-naf-inbox-detail > div > div > div.inbox-detail-content > bsh-naf-customer-search > div > div.customer-form > div.customer-form-side > mat-card.mat-card.mat-focus-indicator.customer-form-notes.customer-form-table > mat-card-title > div.buttons-styles > button.mat-focus-indicator.table-buttons.mat-flat-button.mat-button-base > span.mat-button-wrapper > span')
                .click();

                //radiobutton Billing Address
                cy.wait(2000);
                cy.get('#mat-radio-2 > label > span.mat-radio-container > span.mat-radio-inner-circle').click();

                cy.wait(5000);    
            });
        });
    });
});
*/
    
    it("Customer Email Search",()=>{

        let time="time"

        console.time(time);
       
        cy.visit("http://naf-frontend-1210163857.eu-central-1.elb.amazonaws.com/#/home");
        
        //Click en Interactions
        cy.get('body > bsh-naf-root > div > div > bsh-naf-sidebar > div > div:nth-child(2) > div > span.icon')
        .click();
        
        //Click en el individual profile
        cy.get("body > bsh-naf-root > div > div > div > bsh-naf-inbox > div > div > div > bsh-naf-inbox-detail > div > div > div.inbox-detail-content > bsh-naf-customer-type-select > div > div > div > bsh-naf-outlined-card:nth-child(1) > div > p")
        .click();

        //Búsqueda en el customer search bar
        console.log("Tiempo tras hacer click en interaction y disponerse a escribir el email en la search bar");
        console.timeLog(time)
        console.log('-------------------')


        cy.get('body > bsh-naf-root > div > div > div > bsh-naf-inbox > div > div > div > bsh-naf-inbox-detail > div > div > div.inbox-detail-content > bsh-naf-customer-search > div > div.customer-search > div > input')
        .should('be.visible').type("5")
        cy.wait(1000);
        cy.get('body > bsh-naf-root > div > div > div > bsh-naf-inbox > div > div > div > bsh-naf-inbox-detail > div > div > div.inbox-detail-content > bsh-naf-customer-search > div > div.customer-search > div > span')
        .click();


        //selector de paises
        cy.wait(2000);
        cy.get('.select').should('be.visible').select('Spain')
        cy.wait(2000);
        cy.get('.select').should('be.visible').select('United Kingdom')
        cy.wait(2000);
        cy.get('.select').should('be.visible').select('France')


        //Colapsables history y notes
        cy.wait(2000);
        cy.get('body > bsh-naf-root > div > div > div > bsh-naf-inbox > div > div > div > bsh-naf-inbox-detail > div > div > div.inbox-detail-content > bsh-naf-customer-search > div > div.customer-form > div.customer-form-side > mat-card.mat-card.mat-focus-indicator.customer-form-history.customer-form-table > mat-card-title > div.buttons-styles > button.mat-focus-indicator.table-buttons.mat-flat-button.mat-button-base > span.mat-button-wrapper > span')
        .click()
        cy.wait(2000);
        cy.get('body > bsh-naf-root > div > div > div > bsh-naf-inbox > div > div > div > bsh-naf-inbox-detail > div > div > div.inbox-detail-content > bsh-naf-customer-search > div > div.customer-form > div.customer-form-side > mat-card.mat-card.mat-focus-indicator.customer-form-notes.customer-form-table > mat-card-title > div.buttons-styles > button.mat-focus-indicator.table-buttons.mat-flat-button.mat-button-base > span.mat-button-wrapper > span')
        .click();

        //radiobutton Billing Address
        cy.wait(2000);
        cy.get('#mat-radio-2 > label > span.mat-radio-container > span.mat-radio-inner-circle').click();
        
        console.log("Tiempo final");
        console.timeEnd(time);
        
    
        

 });


