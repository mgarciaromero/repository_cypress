/// <reference types="Cypress"/>

require('cypress-plugin-tab')


describe("test asserts ",()=>{

    it("Demo de los asserts",()=>{

        cy.visit("https://demoqa.com/text-box");
        cy.title().should('eq','ToolsQA');
        cy.wait(1000);

        cy.get('#userName').should('be.visible').type('manu');
        cy.get('#userEmail').should('be.visible').should('be.enabled').type('mgrmanu')

    })

})