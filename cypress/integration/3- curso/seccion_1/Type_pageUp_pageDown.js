/// <reference types="Cypress"/>

require('cypress-plugin-tab')


describe("PageCLICK-ENTER-UP-DOWN ",()=>{

/*    it("Test click enter",()=>{


        cy.visit("https://google.es");
        cy.get("#L2AGLb > div").click();
        cy.wait(2000);
        
        cy.get("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input").type(`demoqa {enter}`)
        cy.wait(3000)

        cy.get('#rso > div:nth-child(1) > div > div > div > div > div > div.yuRUbf > a > h3').click();

    })
*/
    it("Test page up ",()=>{

        cy.visit("https://demoqa.com/text-box");
        cy.title().should('eq','ToolsQA');
        cy.wait(1000)

        cy.get('#userName').type('{pageup}');
    })

    it("Test page down",()=>{

        cy.visit("https://demoqa.com/text-box");
        cy.title().should('eq','ToolsQA');
        cy.wait(1000)

        cy.get('#userName').type('{pagedown}');
    })

    it.only("Tab",()=>{

        cy.visit("https://demoqa.com/text-box");
        cy.title().should('eq','ToolsQA');
        cy.wait(1000)

        cy.get('#userName').type("Manue").tab().type("mgrmanu10@gmail.com").tab().type("calle illo").tab().type("calle illomanue").tab().click();
    })

})