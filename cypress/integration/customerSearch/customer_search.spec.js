/// <reference types="cypress" />

Given('Access via URL to the NAF main window',()=>{

    cy.wait(10000);

    cy.visit({        
        url:"http://naf-frontend-1210163857.eu-central-1.elb.amazonaws.com/#/home",
    });
    cy.wait(2000);

    //cy.get("body > bsh-naf-root > div > div > div > bsh-naf-home > div > bsh-naf-input > label > input").type("aSf6a7dAw8D7as3ae7");

    //cy.reload();

    /*cy.visit({        
        url:"http://naf-frontend-1210163857.eu-central-1.elb.amazonaws.com/#/home",
    });
    */


    cy.wait(2000);

});

When('Access to Interactions',()=>{

    cy.get("body > bsh-naf-root > div > div > bsh-naf-sidebar > div > div:nth-child(2) > div > span.icon").click(); //boton Interactions
})


When('Click On Individual Profile',()=>{
    cy.get("body > bsh-naf-root > div > div > div > bsh-naf-inbox > div > div > div > bsh-naf-inbox-detail > div > div > div.inbox-detail-content > div > bsh-naf-customer-type-select > div > div > div > bsh-naf-outlined-card:nth-child(1) > div")
    .click();
    
    cy.wait(2000);
});

And('Search a valid customer {string}',(customer)=>{

    //Busqueda en la barra de customer identification
    cy.get('.customer-search-container > .ng-untouched')
    .should('be.visible').type(customer);
    cy.wait(3000);
    //click en la lupa
    cy.get('.customer-search-container > .icon')
    .click();
    cy.wait(4000);
    


});

Then('Execute some actions',()=>{

        //selector de paises
        cy.wait(3000);
        cy.get('.select').should('be.visible').select('Spain')
        cy.wait(3000);
        cy.get('.select').should('be.visible').select('United Kingdom')
        cy.wait(3000);
        cy.get('.select').should('be.visible').select('France')


        //Colapsables history y notes
        cy.wait(3000);
        cy.get('.customer-form-history > .mat-card-title > .buttons-styles > .table-buttons')
        .click()
        cy.wait(3000);
        cy.get('.customer-form-notes > .mat-card-title > .buttons-styles > .table-buttons > .mat-button-wrapper > .button-icons')
        .click();

        //radiobutton Billing Address
        cy.wait(3000);
        cy.get('#mat-radio-2 > .mat-radio-label > .mat-radio-container')
        .click();

        cy.wait(3000);
}) ;

