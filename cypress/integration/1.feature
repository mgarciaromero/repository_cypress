Feature: Default

	
	@TSM-15767
	Scenario Outline: Customer_Search (E2E)
		Given Access via URL to the NAF main window
		    When Click On Individual Profile
		    And Search a valid customer '<customer>'
		    Then Execute some actions
		    
		    Examples:
		       | customer            | 
		       | +491707438596 | 
		       | crossbrand.dk.test.2906.v2@bshg.com | 
		       | 696694100 |