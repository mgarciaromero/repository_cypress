Feature: Customer_Search 

    Esto es una demo de como utilizar cocumber

    Scenario Outline: Correct Customer Search by email or number
        Given Access via URL to the NAF main window
        When Click On Individual Profile
        And Search a valid customer '<customer>'
        Then Execute some actions


   # Scenario: Demo de Cocumber
   #     Given Abrir el navegador principal
   #     When Cargando el nombre antonio garcia
   #     When Cargando el email antonio@hotmail.con
   #     And Cargando la dirección
   #     Then Validar el nombre de la página

   Examples:
       | customer | 
       | 649609933 |